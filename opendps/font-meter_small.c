/** Font generated from gfx/nf.ttf 16pt */

#include "font-meter_small.h"

const uint32_t font_meter_small_height = 12;
const uint8_t font_meter_small_pixdata[321] = {
  /* '0', offset=0 */
  0xe0, 0x0b, 0x2c, 0x2c, 0x0d, 0x3c, 0x0e, 0x7d, 0x0b, 0xb7, 0x8b, 0xb2, 0xcb, 0xb0, 0x7b, 0xb0,
  0x3e, 0x70, 0x1d, 0x34, 0x2c, 0x2c, 0xe0, 0x0b,
  /* '1', offset=24 */
  0xf4, 0xc0, 0x3f, 0x20, 0x0d, 0x40, 0x03, 0xd0, 0x00, 0x34, 0x00, 0x0d, 0x40, 0x03, 0xd0, 0x00,
  0x34, 0x00, 0x0d, 0xfc, 0xbf,
  /* '2', offset=45 */
  0xf9, 0x87, 0x41, 0x0b, 0x80, 0x03, 0xd0, 0x00, 0x38, 0x00, 0x0b, 0xf0, 0x00, 0x0f, 0xe0, 0x00,
  0x1e, 0xd0, 0x01, 0xfc, 0xff,
  /* '3', offset=66 */
  0xfd, 0x07, 0x05, 0x2c, 0x00, 0x38, 0x00, 0x34, 0x00, 0x2d, 0xf0, 0x0b, 0x00, 0x2d, 0x00, 0x34,
  0x00, 0x70, 0x00, 0x74, 0x06, 0x3d, 0xfe, 0x0b,
  /* '4', offset=90 */
  0x00, 0x1f, 0x40, 0x1f, 0xc0, 0x1d, 0xa0, 0x1d, 0x30, 0x1d, 0x1c, 0x1d, 0x0d, 0x1d, 0x07, 0x1d,
  0xff, 0xff, 0x00, 0x1d, 0x00, 0x1d, 0x00, 0x1d,
  /* '5', offset=114 */
  0xfd, 0x6f, 0x03, 0xd0, 0x00, 0x34, 0x00, 0xfd, 0x07, 0x41, 0x0b, 0x80, 0x03, 0xd0, 0x00, 0x34,
  0x00, 0x6e, 0xd0, 0xfa, 0x1f,
  /* '6', offset=135 */
  0xe0, 0x2f, 0x38, 0x10, 0x0d, 0x00, 0x0a, 0x00, 0xeb, 0x0b, 0x2f, 0x3c, 0x0f, 0x70, 0x0f, 0xb0,
  0x0e, 0xb0, 0x0d, 0x70, 0x2c, 0x38, 0xe0, 0x0b,
  /* '7', offset=159 */
  0xff, 0x3f, 0x00, 0x0e, 0xc0, 0x02, 0x74, 0x00, 0x0e, 0xc0, 0x02, 0x74, 0x00, 0x0e, 0xc0, 0x02,
  0x34, 0x00, 0x0f, 0xc0, 0x01,
  /* '8', offset=180 */
  0xf4, 0x0b, 0x2d, 0x3c, 0x0e, 0x74, 0x0e, 0x34, 0x1c, 0x2c, 0xf4, 0x0f, 0x1d, 0x38, 0x0f, 0x70,
  0x0b, 0xb0, 0x0f, 0x70, 0x1d, 0x38, 0xf4, 0x0b,
  /* '9', offset=204 */
  0xf4, 0x0b, 0x1d, 0x2d, 0x0f, 0x34, 0x0b, 0x70, 0x0b, 0x70, 0x0f, 0xb4, 0x1d, 0xbc, 0xf4, 0x77,
  0x00, 0x70, 0x00, 0x34, 0x04, 0x1d, 0xfc, 0x07,
  /* '.', offset=228 */
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xc0, 0xff, 0xff,
  /* 'V', offset=237 */
  0x0d, 0x80, 0x73, 0x00, 0xcf, 0x02, 0x1c, 0x0e, 0x34, 0x34, 0xe0, 0xc0, 0xc1, 0x02, 0x0a, 0x07,
  0x34, 0x0d, 0xc0, 0x39, 0x00, 0x7b, 0x00, 0xf8, 0x00, 0xd0, 0x03,
  /* 'A', offset=264 */
  0x40, 0x0f, 0x00, 0x3e, 0x00, 0xec, 0x02, 0x70, 0x0e, 0xd0, 0x34, 0xc0, 0xc3, 0x01, 0x0b, 0x0b,
  0x1d, 0x38, 0xf8, 0xff, 0xb1, 0x00, 0xdb, 0x01, 0xb8, 0x03, 0xd0,
  /* 'W', offset=291 */
  0x0b, 0x00, 0xbb, 0x00, 0x70, 0x0e, 0x00, 0xd7, 0xb0, 0x34, 0x4d, 0x4f, 0xc3, 0xe8, 0x38, 0x9c,
  0x9d, 0xc2, 0x8d, 0x2e, 0xec, 0xe4, 0x81, 0x0f, 0x1f, 0xb4, 0xf0, 0x40, 0x07, 0x0e,
};

const uint8_t font_meter_small_widths[96] = {
    10u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,    3u,    0u,
     8u,    7u,    7u,    8u,    8u,    7u,    8u,    7u,
     8u,    8u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    9u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,    9u,   10u,
     0u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
 };

const uint8_t font_meter_small_sizes[96] = {
     0u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,    9u,    0u,
    24u,   21u,   21u,   24u,   24u,   21u,   24u,   21u,
    24u,   24u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,   27u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,   27u,   30u,
     0u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
 };

const uint16_t font_meter_small_offsets[96] = {
     0u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,  228u,    0u,
     0u,   24u,   45u,   66u,   90u,  114u,  135u,  159u,
   180u,  204u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,  264u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,  237u,  291u,
     0u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
     0u,    0u,    0u,    0u,    0u,    0u,    0u,    0u,
 };

